FROM python:3

WORKDIR /usr/src/app

COPY . .
RUN pip install --no-cache-dir -r requirements.txt
RUN mv /usr/local/lib/python3.7/site-packages/Geohash/ /usr/local/lib/python3.7/site-packages/geohash/
RUN rm /usr/local/lib/python3.7/site-packages/geohash/__init__.py
COPY init_geohash.py /usr/local/lib/python3.7/site-packages/geohash/__init__.py


CMD [ "python", "./app.py" ]

EXPOSE 5000

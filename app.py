#!flask/bin/python

from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS

from algorithms import *
from utils import *
import geohash

print("main")
UPLOAD_FOLDER = '/home/woombatc/jcamilo/venv/uiafc1/RuleConf/ruler'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
api = Api(app)
CORS(app, resources=r'/*', allow_headers=['Content-Type', "Access-Control-Allow-Credentials"])

def init():
    global client_mongo
    global client_psql

    client_mongo = get_mongo_instance()
    client_psql = get_psql_instance()


class Orders(Resource):
    def get(self):
        timestamp = request.args.get('timestamp')
        geohash = request.args.get('geohash')
        order_type = eval(request.args.get('type')) if request.args.get('type') is not None else ["restaurant", "courier", "express", "market", "whim", "express_tablet", "ultraservicio", ]

        orders = get_orders(client_mongo, timestamp, order_type, geohash)
        return list(orders), 200

class Storekeepers(Resource):
    def get(self):
        timestamp = request.args.get('timestamp')
        geohash = request.args.get('geohash')
        vehicle = eval(request.args.get('vehicle')) if request.args.get('vehicle') else [1,0,2]

        storekeepers = get_storekeepers(client_psql, timestamp, vehicle, geohash, None)
        return list(storekeepers), 200

class Recommendation(Resource):
    def get(self):
        timestamp = request.args.get('timestamp')
        storekeeper = request.args.get('storekeeper_id')
        orders = get_orders(client_mongo, timestamp,
                            ["restaurant", "courier", "express", "market", "whim", "express_tablet", "ultraservicio"],
                            None)
        storekeepers = get_storekeepers(client_psql, "'"+timestamp+"'", [1, 0, 2], None, None)

        orders = map(lambda x: {'id': x['id'], 'Q':(x['lat'], x['lng'])},orders)
        storekeepers = map(lambda x: {'storekeeper_id': x['storekeeper_id'], 'Q':(x['lat'], x['lng'])},storekeepers)

        #print(list(storekeepers))
        orders_list = list(orders)
        storekeepers_list = list(storekeepers)
        G, edges = build_graph(orders_list, storekeepers_list, geo_distance, False)
        new_G = prune(G, edges, 0, len(orders_list), len(storekeepers_list))
        #return G[storekeeper]
        return new_G

class Saturation(Resource):
    def get(self):
        timestamp = request.args.get('timestamp')

        orders = get_orders(client_mongo, timestamp,
                            ["restaurant", "courier", "express", "market", "whim", "express_tablet", "ultraservicio"],
                            None)
        storekeepers = get_storekeepers(client_psql, "'"+timestamp+"'", [1, 0, 2], None, None)

        orders = map(lambda order: (geohash_encode(order['lat'], order['lng'], 5), 1), orders)

        storekeepers = map(lambda storekeeper: (geohash_encode(storekeeper['lat'], storekeeper['lng'], 5), 1), storekeepers )

        #print((orders1))

        grouped_orders = reduce_counter(list(orders))
        grouped_storekeepers = reduce_counter(list(storekeepers))

        result = []
        for geohash_site in grouped_orders:
            n_orders = grouped_orders[geohash_site]
            if geohash_site in grouped_storekeepers:
                n_storekeepers = grouped_storekeepers[geohash_site]
            else:
                n_storekeepers = 1
            position = geohash.decode_exactly(geohash_site)
            result.append( {'lat': position[0], 'lng': position[1], 'weight': n_orders/n_storekeepers})
        return result

@app.route('/')
def index():
    return "Hello, World!"

if __name__ == '__main__':
    init()
    api.add_resource(Storekeepers, '/storekeepers')
    api.add_resource(Orders, '/orders')
    api.add_resource(Recommendation, '/recommendation')
    api.add_resource(Saturation, '/saturation')
    app.run(port=5000, host='0.0.0.0')

import math
import heapq

def euclidean_distance(A, B):
    return math.pow(A[0] - B[0], 2) + math.pow(A[1] - B[1], 2)


def defined_distance( A, B ):
    return

def calculate_locked(G, deleted, locked, delete_count):
    order, storekeeper = deleted[1], deleted[2]
    if((order, storekeeper) not in locked and (storekeeper,order) not in locked or True):
        G[order].pop(storekeeper, None)
        G[storekeeper].pop(order, None)
        if len(G[order]) == 1:
            locked[(order, [e for e in G[order]][0])] = True
        if len(G[storekeeper]) == 1:
            locked[([e for e in G[storekeeper]][0], storekeeper)] = True
        delete_count += 1
    return locked, delete_count

def calculate_locked_greedy(G, deleted, delete_count):
    order, storekeeper = deleted[1], deleted[2]
    if(storekeeper in G[order] and order in G[storekeeper]):
        G[order] = {}
        G[order][storekeeper] = storekeeper
        G[storekeeper] = {}
        G[storekeeper][order] = order
        delete_count += 1
    return delete_count

def prune(G, edges, punish_hard, num_orders, num_storekeepers):
    locked = {}
    delete_count = 0
    heapq.heapify(edges)
    print(int(num_orders) * int(num_storekeepers))
    print( (num_orders, num_storekeepers))
    while num_orders * num_storekeepers - delete_count > min(num_orders, num_storekeepers):
        if len(edges) != 0:
            deleted = heapq.heappop(edges)
            locked, delete_count = calculate_locked(G, deleted, locked, delete_count)
        else: return G
    print(delete_count)
    return G

def greedy(G, edges, punish_hard, num_orders, num_storekeepers):
    delete_count = 0
    while delete_count < min(num_orders, num_storekeepers):
        deleted = heapq.heappop(edges)
        delete_count = calculate_locked_greedy(G, deleted, delete_count)
    return G

def build_graph( orders, storekeepers, distance_metric, greedy ):
    G = {}
    edges = []
    for order in orders:
        order_id = "O" + str(order['id'])
        if(order_id not in G): G[order_id] = {}
        for storekeeper in storekeepers:
            storekeeper_id = "S" + str(storekeeper['storekeeper_id'])
            if storekeeper_id  not in G: G[storekeeper_id ] = {}
            G[order_id][storekeeper_id] = storekeeper_id
            G[storekeeper_id ][order_id] = order_id
            mult = 1 if greedy else -1
            edges.append((mult*distance_metric(order['Q'], storekeeper['Q']), order_id, storekeeper_id ))
    return G, edges

if __name__ == "__main__":
    orders = [{'id': 1, 'Q': (0,0)}, {'id': 2, 'Q': (10,10)}]
    storekeepers = [{'id': 1, 'Q': (1,0)}, {'id': 2, 'Q': (-3,2)}]

    #orders = [{'id': 0, 'Q': (0,0)}, {'id': 1, 'Q': (1,6)}]
    #storekeepers = [{'id': 1, 'Q': (6,2)}, {'id': 2, 'Q': (13,4)}, {'id': 2, 'Q': (16,8)}]
    G, edges = build_graph(orders, storekeepers, euclidean_distance, True)
    print (G)
    new_G = prune(G, edges, 0, len(orders), len(storekeepers))
    print(new_G)
